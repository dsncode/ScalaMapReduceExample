package basic

class Person(val name : String,
             val age: Int,
             val nationality : String = "taiwan")

object Person{

  def getType() = "person"
}

object FilterExample extends App {

  val ls = List(1,2,3,4,5,6,7,8,9,10)

  def filterOdd(x:Int) = x % 2 ==1

  ls.filter(filterOdd)
    .foreach(println)

}
