package mapreduce.functional

object ReduceExample2 extends App{

  val numbers = List(1,2,3,4,5,6)

  val total = numbers.reduce((a,b)=>a+b)
  println(total)

  println("reduce LEFT:")
  val leftReduce = numbers.reduceLeft((acc, x)=>{
    val total = acc-x
    println(s"acc:$acc ~ action: $acc - $x = $total")
    total
  })
  println(leftReduce)
  println("reduce RIGHT:")
  val rightReduce = numbers.reduceRight((x, acc)=>{
    val total = acc-x
    println(s"acc:$acc ~ action: $acc - $x = $total")
    total
  })
  println(rightReduce)
}
