package mapreduce.functional

object MapExample extends App{

  def mutiplyBy2(x:Int): Int ={
    return x * 2
  }

  val range = (0 to 100)

  range.map(mutiplyBy2).foreach(println)

}
