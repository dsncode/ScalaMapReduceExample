package mapreduce.functional

object FilterExample extends App{


  def isOddNumber(x:Int): Boolean ={
    if(x%2==0){
      return true
    }else{
      return false
    }
  }

  val range = (0 to 1000)

  range.filter(isOddNumber).foreach(println)


}
