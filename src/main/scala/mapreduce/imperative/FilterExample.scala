package mapreduce.imperative

object FilterExample extends App{

  def isOddNumber(x:Int): Boolean ={

    if(x%2==0){
      return true
    }else{
      return false
    }

  }


  val range = (0 to 1000)
  val oddList = scala.collection.mutable.ListBuffer[Int]()

  for(index <- range){
    val odd = isOddNumber(index)
    if(odd){
      oddList.append(index)
    }
  }

  // print all odd numbers
  for(index <- oddList){
      println(index)
  }



}
