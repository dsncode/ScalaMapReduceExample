package mapreduce.functional

import scala.io.Source

object MostPopularMovie extends App{




  val lines  = Source.fromFile("movies.csv").getLines()
  val seqLines = lines.toList

  val start = System.currentTimeMillis()
  val summary = seqLines
            .map(line=>{
              val arr = line.split(",")
              arr.last
            })
            .filter(line=>line.contentEquals("(no genres listed)") == false)
            .map(line=>line.split("\\|").toList)
            .flatMap(wordList=>wordList)
            .groupBy(wordList=>wordList)
            .mapValues(values=>values.size)
            .toList.sortBy(tuple=>tuple._2)(Ordering[Int].reverse)


  summary.foreach(println)
  println(s"time taken ${System.currentTimeMillis()-start}ms")


}
