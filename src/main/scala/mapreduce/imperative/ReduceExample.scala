package mapreduce.imperative

object ReduceExample extends App{


  val range = (0 to 100)

  var acc = 0
  for(sample <- range){
    acc = acc + sample
  }


  println(acc)
}
