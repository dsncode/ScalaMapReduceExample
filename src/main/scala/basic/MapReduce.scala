package basic

import scala.io.Source

object MapReduce extends App{

  val it = Source.fromFile("movies.csv")
            .getLines()
            .map(line=>line.split(","))
            .map(arr=>(arr(0),arr(1)))

  it.foreach(println)


}
