package week1

import java.io.{File, FileWriter, PrintWriter}

import scalaj.http.HttpOptions

case class MovieMeta(val id:String, val name:String, val link:String, val httpStatus:Int)

object Movies extends App{

  // load all csv links into a linksDB hashmap
  val linksDB = readFile("links.csv").map(line=>{
                                                  val split = line.split(",")
                                                  (split(0), split(1))
                                                })
                                            .groupBy(_._1)
                                            .mapValues(_.head._2)

  // read a file from a path
  def readFile(path : String) : List[String] = scala.io.Source.fromFile(path).getLines().toList

  def lineToMovieMeta(line:String): MovieMeta ={
    val split = line.split(",")
    MovieMeta(split(0), split(1), "", 0)
  }

  // transform a movie id to a imdb url
  def addIMDBUrl(movieMeta:MovieMeta) = {
    val imdbId = linksDB.get(movieMeta.id).getOrElse("")
    val imbdLink = s"https://www.imdb.com/title/tt${imdbId}"
    MovieMeta(movieMeta.id, movieMeta.name, imbdLink, 0)
  }

  def saveToFile(writer:FileWriter) : MovieMeta => Unit = (movie) => writer.write(s"${movie.id}, ${movie.name}, ${movie.link}, ${movie.httpStatus}\n")

  def checkAndAddIMDBStatus(movieMeta: MovieMeta): MovieMeta ={

    try {

      val response = scalaj.http.Http(movieMeta.link)
        .option(HttpOptions.followRedirects(true))
        .option(HttpOptions.connTimeout(10000))
        .option(HttpOptions.readTimeout(10000))
        .asString
      println(s"checking ${movieMeta.name} on ${movieMeta.link} [${response.code}]")
      MovieMeta(movieMeta.id, movieMeta.name,movieMeta.link, response.code)
    }
    catch{
      case _ : java.net.SocketTimeoutException => println(s"TIMEOUT ${movieMeta.name}");MovieMeta(movieMeta.id, movieMeta.name,movieMeta.link, 500)
    }
  }

  // Code challenge: build a transformation pipeline
  // background: we will be using movielens dataset having x movies (complete task 1 to know the number).
  // the goal, is to check on IMBD if their ratings exists, also, to prepare a report with their links and http status.

  //1. find the total amount of movies in file movies.csv

  //2. transform a line into a MovieMeta object

  //3. find the IMBD url for all movies, save it into MovieMeta object

  //4. check if the url exists on IMDB, keep your findings into MovieMeta object

  //5. output a report into movies_imdb_links.csv
  // movie_id, movie_name, imdb_link, http_status (400 not found, 200 ok)
  // ex: 1, Toy Story (1995), https://www.imdb.com/title/tt0114709, 200

  //6. how many links are valid? how many are not?




}
