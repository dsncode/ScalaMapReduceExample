package basic

import org.scalatest._

class FilterHomeworkWeek1Test extends FlatSpec with Matchers {

  val numberFilter = new week1.homework.FilterHomework()

  "filter even, " should " keep only even numbers" in {

    val even = (0 to 100).toList.filter(numberFilter.keepEvenNumber)

    for(num <- even){
      num % 2 shouldEqual 0
    }

  }

  "filter odd, " should " keep only odd numbers" in {

    val odd = (0 to 100).toList.filter(numberFilter.keepODDNumber)

    for(num <- odd){
      num % 2 shouldEqual 1
    }

  }

  "filter number, " should " keep only numbers" in {

    val even = List("a", 1, 2,'c', List()).filter(numberFilter.keepNumber)

    for(num <- even){
      num match{
        case x : Integer => ""
        case _ => fail(s"$num is not a number...")
      }
    }

  }

  "keep existent object filter" should " keep only existent objects" in {
    val existent = List(Option.empty, Option.apply(1), Option.empty, Option.apply('x'))
        .filter(numberFilter.keepExistentObjects)

    existent.foreach(_ match{
      case None => fail("there is an not existent object")
      case Some(_) => ""
    })

  }
}
