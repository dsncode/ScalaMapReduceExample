package mapreduce.imperative

object MapExample extends App{

  def mutiplyBy2(x:Int): Int ={
    return x * 2
  }



  val range = (0 to 100)

  val acc = scala.collection.mutable.ListBuffer[Int]()

  for(sample <- range){
    val numx2 = mutiplyBy2(sample)
    acc.append(numx2)
  }


  for(sample <- acc){
    println(sample)
  }

}
