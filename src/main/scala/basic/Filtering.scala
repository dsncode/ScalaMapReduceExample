package basic

object Filtering extends App {


  def isEvenNumber(number: Int): Boolean = {
    if (number % 2 == 0) {
      return true
    }
    else {
      return false
    }
  }

  def isOddNumber(number: Int): Boolean = {
    if (number % 2 == 1) {
      return true
    }
    else {
      return false
    }
  }

  val numbers = (0 to 100)

  val tuples = List(
    (1,3),(5,3),(0,7),(4,7),(2,5)
  )
  val tuple2 = List(
    "a","b","c","d","z"
  )

  def multiplyBy2(x : Int): Int = x * 2

  tuples.filter(tuple=>{

    if(tuple._1%2==0 && tuple._2>5){
       true
    }else{
      false
    }

  }).foreach(println)

}
