package week1.homework


class FilterHomework {

  // keep odd numbers
  def keepODDNumber(number:Int) : Boolean = ???

  // keep even numbers
  def keepEvenNumber(number:Int) : Boolean = ???

  // keep numbers only
  def keepNumber(obj:Any) : Boolean = ???

  // keep only existing objects
  // hint, check Option class methods...
  def keepExistentObjects[A](option: Option[A]) : Boolean = ???

}
